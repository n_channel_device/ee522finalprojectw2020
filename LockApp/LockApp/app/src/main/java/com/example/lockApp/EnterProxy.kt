package com.example.lockApp

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import android.widget.Toast.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.proxy_enter.*
import org.json.JSONObject.NULL

class EnterProxy : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.proxy_enter)
    }

    fun clickOK (view : View) {
        val PROXY_URL = proxy_url.text.toString()
        // check to confirm the proxy is valid. If not make them try again
        val intent = Intent()

        intent.putExtra("PROX", PROXY_URL)

        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    fun launchRemoteIT (view : View) {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse("https://remote.it/")
        startActivity(openURL)
    }
}