package com.example.lockApp

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.menu_lock.*
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.lang.ref.WeakReference
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.URL

class LockMenu : AppCompatActivity() {

    lateinit var proxyURL : String

    // filename of proxy.txt
    private val proxyFile: String = "proxy.txt"
    private var firstTime = false

    // flag for state of lock (should be locked by default
    var isLocked = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_lock)
        // if file for proxy.txt does not exist, create it
        // then pass intent to EnterProxy to input proxy into file
        // then save on return from EnterProxy
        val file = baseContext.getFileStreamPath(proxyFile)
        if (!file.exists()) {
            firstTime = true
            val intentProxy = Intent(this, EnterProxy::class.java)
            startActivityForResult(intentProxy, PROXY_INFO)
        } else {
            proxyURL = readFile(proxyFile)
        }
    }

    override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {
        // Check which request we're responding to
        if (requestCode == PROXY_INFO) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                proxyURL = data!!.getStringExtra("PROX")
                // if this is the first time we are writing proxy info to file
                if (firstTime) {
                    writeFile(proxyFile,proxyURL)
                    firstTime = false
                } else { // not first time so delete current file and write new proxy
                    deleteFile(proxyFile)
                    writeFile(proxyFile,proxyURL)
                }
            }
        }
    }

    fun enterProxy(view: View) {
        val intentProxy = Intent(this, EnterProxy::class.java)

        startActivityForResult(intentProxy, PROXY_INFO)
    }

    // This function handles the button click listener for lock/unlock latch
    @SuppressLint("ResourceAsColor", "ResourceType")
    fun toggleLock(view: View) {

        // toggle to unlocked if current state is locked
        if (isLocked) {
            Background_get(this).execute(proxyURL, "1/UNLOCK")
            buToggleLock.setText(R.string.unlockText)
            buToggleLock.setBackgroundResource(R.drawable.red_bu)
            isLocked = false
        } else {
            Background_get(this).execute(proxyURL, "1/LOCK")
            buToggleLock.setText(R.string.lockedText)
            buToggleLock.setBackgroundResource(R.drawable.green_bu)
            isLocked = true
        }

    }

    // write file to internal app storage
    // from Laura Aronja EE 590 EE PMP Spring 2019
    private fun writeFile(filename : String, content : String) {

        this.openFileOutput(filename, Context.MODE_PRIVATE).use {
            it.write(content.toByteArray())
        }
    }

    // read file from internal app storage
    // from Laura Aronja EE 590 EE PMP Spring 2019
    private fun readFile(filename : String) : String {
        val file = File(this.filesDir, filename)
        val contents = file.readLines() // Read file by lines

        return contents[0]
    }

    companion object {
        // these "request codes" are used to identify sub-activities that return results
        private const val PROXY_INFO = 28

        // Request code for creating a txt document for proxy storage
        private const val CREATE_FILE = 1

        // Request code for creating a txt document for proxy storage
        private const val OPEN_FILE = 2

        /********************************************************************************/
        /**  This is a background process for connecting                                */
        /**   to the arduino server and sending                                         */
        /**    the GET request withe the added data                                     */
        /**      Adapted from MainActivity.java implementation                          */
        /**     courtesy of Laurens_Wyuts on instructables.com                          */
        /**    https://www.instructables.com/id/Control-Arduino-using-android-app/#step5*/
        /********************************************************************************/

        internal class Background_get internal constructor(context: LockMenu) : AsyncTask<String?, Void?, String?>() {

            private var resp: String? = null
            private val activityReference: WeakReference<LockMenu> = WeakReference(context)

            override fun onPreExecute() {
                val activity = activityReference.get()
                if (activity == null || activity.isFinishing) return
            }

            override fun doInBackground(params: Array<String?>): String? {
                try {
                    /** */ /* Change the IP to the IP you set in the arduino sketch */
                    /** */
                    val url = URL(params[0] + params[1])
                    val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
                    val `in` =
                        BufferedReader(InputStreamReader(connection.inputStream))
                    val result = StringBuilder()
                    var inputLine: String?
                    while (`in`.readLine().also {
                            inputLine = it
                        } != null) result.append(inputLine).append("\n")
                    `in`.close()
                    connection.disconnect()
                    resp = result.toString()
                } catch (e: ConnectException) {
                    resp = "NO_CONNECT"
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                return resp
            }
            override fun onPostExecute(result: String?) {

                val activity = activityReference.get()
                if (activity == null || activity.isFinishing) return
                if (result == "NO_CONNECT") {
                    Toast.makeText(activity,"Remote.it Proxy URL INVALID. Please enter a new one.",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}