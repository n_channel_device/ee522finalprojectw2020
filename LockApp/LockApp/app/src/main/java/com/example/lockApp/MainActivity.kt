package com.example.lockApp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.lockApp.OnSwipeListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intentLockMenu = Intent(this, LockMenu::class.java)
        activity_main.setOnTouchListener(object : OnSwipeListener(this) {

            init {
                setDragHorizontal(true)
                setExitScreenOnSwipe(true)
                setAnimationDelay(500)
            }

            override fun onSwipeLeft(distance: Float) {
                startActivity(intentLockMenu)
            }

            override fun onSwipeRight(distance: Float) {
                startActivity(intentLockMenu)
            }
        })
    }
}
