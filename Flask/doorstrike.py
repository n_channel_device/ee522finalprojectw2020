from flask import Flask, render_template, request
app = Flask(__name__)

# Create a dictionary called pins to store the pin number, name, and pin state:
# implement up to three for future expansion
locks = {
   1 : {'name' : 'main', 'state' : 'LOCK'},
   2 : {'name' : 'second', 'state' : 'LOCK'},
   3 : {'name' : 'third', 'state' : 'LOCK'}
   }

@app.route("/")
def main():
   
   # Put the pin dictionary into the template data dictionary:
   templateData = {
      'locks' : locks
      }
   # Pass the template data into the template main.html and return it to the user
   return render_template('main.html', **templateData)

# The function below is executed when someone requests a URL with the pin number and action in it:
@app.route("/<changeLock>/<action>")
def action(changeLock, action):
   # Convert the pin from the URL into an integer:
   changeLock = int(changeLock)
   # Get the device name for the pin being changed:
   deviceName = locks[changeLock]['name']
   # If the action part of the URL is "on," execute the code indented below:
   if action == "UNLOCK":
        #change lock state to unlocked
      locks[changeLock]['state'] = 'UNLOCK'
      # Save the status message to be passed into the template:
      message = "Unlocked " + deviceName
   if action == "LOCK":
      # change to lock for given lock
      locks[changeLock]['state'] = 'LOCK'
      message = "LOCKED " + deviceName
   if action == "READ":
      
      message = deviceName + " state: " + locks[changeLock]['state']

   # Along with the pin dictionary, put the message into the template data dictionary:
   templateData = {
      'message' : message,
      'locks' : locks
   }

   return render_template('main.html', **templateData)

if __name__ == "__main__":
   app.run(host='0.0.0.0', port=8910, debug=True)