#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>

// primative type used since expected by GPIO function
int mainPin = 5;

// Set String for door lock types and valid lock states
String mainLock = "main";
String secLock = "second";
String thrLock = "third";
String stateLock = "LOCK";
String stateUnlock = "UNLOCK";

void setup() {
  Serial.begin(115200);
  WiFi.begin("Cerberus", "shinkAns3n");
 
  while (WiFi.status() != WL_CONNECTED) {
 
    delay(1000);
    Serial.println("Connecting..");
 
  }
  Serial.println("Connected to WiFi Network");

  // init pin(s). Only one pin is enabled at this time
  pinMode(mainPin, OUTPUT);
}

void loop() {
  // Different read states for the three different locks defined by URIs
  // RPi has internal LAN IPv4 addr 192.169.1.17
  // Port 8910 is used by Flask Server for communicating on network
  String readState1 = "http://192.168.1.17:8910/1/READ";
  String readState2 = "http://192.168.1.17:8910/2/READ";
  String readState3 = "http://192.168.1.17:8910/3/READ";
  
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
    // keep original html buffer for safety
    String text = sendGET(readState1);

    // know that html buffer first 34 char always dump
    // know that all html char after "K" will be dumped
    String mod = text;
    mod.remove(0,34);
    mod.remove(mod.indexOf("K") + 1);

    // figure out the door and state
    String door = mod;
    String state = mod;
    door.remove(door.indexOf(" "));
    state.remove(0,state.indexOf(":") + 2);

    // If this is the main door lock has state UNLOCK, 
    // activate electric strike plate.
    // If server state is Locked, de-activate the lock
    if (door == mainLock) {
      if (state == stateUnlock) {
        digitalWrite(mainPin, HIGH);
      }
      if (state == stateLock) {
        digitalWrite(mainPin, LOW);
      }
    }
 
  }
 
  delay(1000);    //Send a request every 1 second heartbeat

}

// this is the method for making GET requests using HTTP commands
// since we are contacting the server, we instantiate ourselves as a HTTP client
// Then we attempt to resolve the URL and send GET request
// If the server on RPi is up, then the getString() should return the
// html dump being sent by the server to be rendered by a browser
// we will not render with browser but extract relevant info
String sendGET(String URLGET) {
  HTTPClient http;  //Declare an object of class HTTPClient
  
  http.begin(URLGET); //Specify request destination
 
    int httpCode = http.GET(); //Send the request
    String payload;
    if (httpCode > 0) { //Check the returning code
 
      payload = http.getString();   //Get the request response payload
 
    }else Serial.println("An error ocurred");

    http.end();   //Close connection
    return payload;
}
